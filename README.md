# A collection of .scl tuning files created by Elisabeth Bohnert.

## Temperament Tables
the following tables show how the fifths and thirds deviate from pure 3/2 and 5/4 ratios, respectively, in each of the .scl files. Unlabeled fractions represent a proportion of the Pythagorean comma, the letter z represents the syntonic comma, the letter ſ represents the schisma, and a dash marks an interval as pure

### 3mean.scl &mdash; 1/3-Comma Meantone
|       |               |       |               |
|-------|---------------|-------|---------------|
| C-G   | -1&frasl;3z   | C-E   | -1&frasl;3z   |
| G-D   | -1&frasl;3z   | G-B   | -1&frasl;3z   |
| D-A   | -1&frasl;3z   | D-F#  | -1&frasl;3z   |
| A-E   | -1&frasl;3z   | A-C#  | -1&frasl;3z   |
| E-B   | -1&frasl;3z   | E-G#  | -1&frasl;3z   |
| B-F#  | -1&frasl;3z   | B-Eb  | +8&frasl;3z-ſ |
| F#-C# | -1&frasl;3z   | F#-Bb | +8&frasl;3z-ſ |
| C#-G# | -1&frasl;3z   | C#-F  | +8&frasl;3z-ſ |
| G#-Eb | +8&frasl;3z-ſ | G#-C  | +8&frasl;3z-ſ |
| Eb-Bb | -1&frasl;3z   | Eb-G  | -1&frasl;3z   |
| Bb-F  | -1&frasl;3z   | Bb-D  | -1&frasl;3z   |
| F-C   | -1&frasl;3z   | F-A   | -1&frasl;3z   |

### 4mean.scl &mdash; 1/4-Comma Meantone
|       |               |       |         |
|-------|---------------|-------|---------|
| C-G   | -1&frasl;4z   | C-E   | &mdash; |
| G-D   | -1&frasl;4z   | G-B   | &mdash; |
| D-A   | -1&frasl;4z   | D-F#  | &mdash; |
| A-E   | -1&frasl;4z   | A-C#  | &mdash; |
| E-B   | -1&frasl;4z   | E-G#  | &mdash; |
| B-F#  | -1&frasl;4z   | B-Eb  | +2z-ſ   |
| F#-C# | -1&frasl;4z   | F#-Bb | +2z-ſ   |
| C#-G# | -1&frasl;4z   | C#-F  | +2z-ſ   |
| G#-Eb | +7&frasl;4z-ſ | G#-C  | +2z-ſ   |
| Eb-Bb | -1&frasl;4z   | Eb-G  | &mdash; |
| Bb-F  | -1&frasl;4z   | Bb-D  | &mdash; |
| F-C   | -1&frasl;4z   | F-A   | &mdash; |

### 5mean.scl &mdash; 1/5-Comma Meantone
|       |               |       |               |
|-------|---------------|-------|---------------|
| C-G   | -1&frasl;5z   | C-E   | +1&frasl;5z   |
| G-D   | -1&frasl;5z   | G-B   | +1&frasl;5z   |
| D-A   | -1&frasl;5z   | D-F#  | +1&frasl;5z   |
| A-E   | -1&frasl;5z   | A-C#  | +1&frasl;5z   |
| E-B   | -1&frasl;5z   | E-G#  | +1&frasl;5z   |
| B-F#  | -1&frasl;5z   | B-Eb  | +8&frasl;5z-ſ |
| F#-C# | -1&frasl;5z   | F#-Bb | +8&frasl;5z-ſ |
| C#-G# | -1&frasl;5z   | C#-F  | +8&frasl;5z-ſ |
| G#-Eb | +6&frasl;5z-ſ | G#-C  | +8&frasl;5z-ſ |
| Eb-Bb | -1&frasl;5z   | Eb-G  | +1&frasl;5z   |
| Bb-F  | -1&frasl;5z   | Bb-D  | +1&frasl;5z   |
| F-C   | -1&frasl;5z   | F-A   | +1&frasl;5z   |

### 6mean.scl &mdash; 1/6-Comma Meantone
|       |               |       |              |
|-------|---------------|-------|--------------|
| C-G   | -1&frasl;6z   | C-E   | +1&frasl;3z  |
| G-D   | -1&frasl;6z   | G-B   | +1&frasl;3z  |
| D-A   | -1&frasl;6z   | D-F#  | +1&frasl;3z  |
| A-E   | -1&frasl;6z   | A-C#  | +1&frasl;3z  |
| E-B   | -1&frasl;6z   | E-G#  | +1&frasl;3z  |
| B-F#  | -1&frasl;6z   | B-Eb  | +4&frasl;3-ſ |
| F#-C# | -1&frasl;6z   | F#-Bb | +4&frasl;3-ſ |
| C#-G# | -1&frasl;6z   | C#-F  | +4&frasl;3-ſ |
| G#-Eb | +5&frasl;6z-ſ | G#-C  | +4&frasl;3-ſ |
| Eb-Bb | -1&frasl;6z   | Eb-G  | +1&frasl;3z  |
| Bb-F  | -1&frasl;6z   | Bb-D  | +1&frasl;3z  |
| F-C   | -1&frasl;6z   | F-A   | +1&frasl;3z  |

### et-bohlen-pierce.scl &mdash; Bohlen-Pierce (Equal Tempered)
Chart not applicable, not a 12-tone tuning.

### jorg.scl &mdash; Jorgensen Five-and-Seven
note: Jorgensen tuning is an interlaced ET system, rather than being built on tempered fifths. For this reason, this chart is in cents rather than commas. This chart is not representative of how this tuning is intended to be used, it is simply here for completeness and for comparison.
|       |        |       |        |
|-------|--------|-------|--------|
| C-G   | -16.3¢ | C-E   | -43.7¢ |
| G-D   | -16.3¢ | G-B   | -43.7¢ |
| D-A   | -16.3¢ | D-F#  | -26.5¢ |
| A-E   | -16.3¢ | A-C#  | +7.8¢  |
| E-B   | -16.3¢ | E-G#  | +42.1¢ |
| B-F#  | +0.9¢  | B-Eb  | +54.9¢ |
| F#-C# | +18¢   | F#-Bb | +93.5¢ |
| C#-G# | +18¢   | C#-F  | +54.9¢ |
| G#-Eb | +18¢   | G#-C  | +42.1¢ |
| Eb-Bb | +18¢   | Eb-G  | +7.8¢  |
| Bb-F  | +0.9¢  | Bb-D  | -26.5¢ |
| F-C   | -16.3¢ | F-A   | -43.7¢ |

### kell.scl &mdash; Kellner
|       |            |       |              |
|-------|------------|-------|--------------|
| C-G   | -1&frasl;5 | C-E   | +1&frasl;5-ſ |
| G-D   | -1&frasl;5 | G-B   | +2&frasl;5-ſ |
| D-A   | -1&frasl;5 | D-F#  | +2&frasl;5-ſ |
| A-E   | -1&frasl;5 | A-C#  | +3&frasl;5-ſ |
| E-B   | &mdash;    | E-G#  | +4&frasl;5-ſ |
| B-F#  | -1&frasl;5 | B-Eb  | +4&frasl;5-ſ |
| F#-C# | &mdash;    | F#-Bb | +z           |
| C#-G# | &mdash;    | C#-F  | +z           |
| G#-Eb | &mdash;    | G#-C  | +z           |
| Eb-Bb | &mdash;    | Eb-G  | +4&frasl;5-ſ |
| Bb-F  | &mdash;    | Bb-D  | +3&frasl;5-ſ |
| F-C   | &mdash;    | F-A   | +2&frasl;5-ſ |

### kirn1.scl &mdash; Kirnberger I
|       |         |       |         |
|-------|---------|-------|---------|
| C-G   | &mdash; | C-E   | &mdash; |
| G-D   | &mdash; | G-B   | &mdash; |
| D-A   | -z      | D-F#  | &mdash; |
| A-E   | &mdash; | A-C#  | +z-ſ    |
| E-B   | &mdash; | E-G#  | +z-ſ    |
| B-F#  | &mdash; | B-Eb  | +z-ſ    |
| F#-C# | -ſ      | F#-Bb | +z-ſ    |
| C#-G# | &mdash; | C#-F  | +z      |
| G#-Eb | &mdash; | G#-C  | +z      |
| Eb-Bb | &mdash; | Eb-G  | +z      |
| Bb-F  | &mdash; | Bb-D  | +z      |
| F-C   | &mdash; | F-A   | &mdash; |

### kirn2.scl &mdash; Kirnberger II
|       |             |       |             |
|-------|-------------|-------|-------------|
| C-G   | &mdash;     | C-E   | &mdash;     |
| G-D   | -1&frasl;2z | G-B   | &mdash;     |
| D-A   | -1&frasl;2z | D-F#  | +1&frasl;2z |
| A-E   | &mdash;     | A-C#  | +z-ſ        |
| E-B   | &mdash;     | E-G#  | +z-ſ        |
| B-F#  | &mdash;     | B-Eb  | +z-ſ        |
| F#-C# | -ſ          | F#-Bb | +z-ſ        |
| C#-G# | &mdash;     | C#-F  | +z          |
| G#-Eb | &mdash;     | G#-C  | +z          |
| Eb-Bb | &mdash;     | Eb-G  | +z          |
| Bb-F  | &mdash;     | Bb-D  | +1&frasl;2z |
| F-C   | &mdash;     | F-A   | &mdash;     |

### kirn3.scl &mdash; Kirnberger III
|       |             |       |               |
|-------|-------------|-------|---------------|
| C-G   | -1&frasl;4z | C-E   | &mdash;       |
| G-D   | -1&frasl;4z | G-B   | +1&frasl;4z   |
| D-A   | -1&frasl;4z | D-F#  | +1&frasl;2z   |
| A-E   | -1&frasl;4z | A-C#  | +3&frasl;4z-ſ |
| E-B   | &mdash;     | E-G#  | +z-ſ          |
| B-F#  | &mdash;     | B-Eb  | +z-ſ          |
| F#-C# | -ſ          | F#-Bb | +z-ſ          |
| C#-G# | &mdash;     | C#-F  | +z            |
| G#-Eb | &mdash;     | G#-C  | +z            |
| Eb-Bb | &mdash;     | Eb-G  | +3&frasl;4z   |
| Bb-F  | &mdash;     | Bb-D  | +1&frasl;2z   |
| F-C   | &mdash;     | F-A   | +1&frasl;4z   |

### pyth.scl &mdash; Pythagorean Tuning
|       |         |       |    |
|-------|---------|-------|----|
| C-G   | &mdash; | C-E   | +z |
| G-D   | &mdash; | G-B   | +z |
| D-A   | &mdash; | D-F#  | +z |
| A-E   | &mdash; | A-C#  | +z |
| E-B   | &mdash; | E-G#  | +z |
| B-F#  | &mdash; | B-Eb  | -ſ |
| F#-C# | &mdash; | F#-Bb | -ſ |
| C#-G# | &mdash; | C#-F  | -ſ |
| G#-Eb | -1      | G#-C  | -ſ |
| Eb-Bb | &mdash; | Eb-G  | +z |
| Bb-F  | &mdash; | Bb-D  | +z |
| F-C   | &mdash; | F-A   | +z |

### val.scl &mdash; Valotti
|       |            |       |              |
|-------|------------|-------|--------------|
| C-G   | -1&frasl;6 | C-E   | +1&frasl;3-ſ |
| G-D   | -1&frasl;6 | G-B   | +1&frasl;3-ſ |
| D-A   | -1&frasl;6 | D-F#  | +1&frasl;2-ſ |
| A-E   | -1&frasl;6 | A-C#  | +2&frasl;3-ſ |
| E-B   | -1&frasl;6 | E-G#  | +5&frasl;6-ſ |
| B-F#  | &mdash;    | B-Eb  | +z           |
| F#-C# | &mdash;    | F#-Bb | +z           |
| C#-G# | &mdash;    | C#-F  | +z           |
| G#-Eb | &mdash;    | G#-C  | +5&frasl;6-ſ |
| Eb-Bb | &mdash;    | Eb-G  | +2&frasl;3-ſ |
| Bb-F  | &mdash;    | Bb-D  | +1&frasl;2-ſ |
| F-C   | -1&frasl;6 | F-A   | +1&frasl;3-ſ |

### werck3.scl &mdash; Werckmeister III
|       |            |       |              |
|-------|------------|-------|--------------|
| C-G   | -1&frasl;4 | C-E   | +1&frasl;4-ſ |
| G-D   | -1&frasl;4 | G-B   | +1&frasl;2-ſ |
| D-A   | -1&frasl;4 | D-F#  | +1&frasl;2-ſ |
| A-E   | &mdash;    | A-C#  | +3&frasl;4-ſ |
| E-B   | &mdash;    | E-G#  | +3&frasl;4-ſ |
| B-F#  | -1&frasl;4 | B-Eb  | +3&frasl;4-ſ |
| F#-C# | &mdash;    | F#-Bb | +z           |
| C#-G# | &mdash;    | C#-F  | +z           |
| G#-Eb | &mdash;    | G#-C  | +z           |
| Eb-Bb | &mdash;    | Eb-G  | +3&frasl;4-ſ |
| Bb-F  | &mdash;    | Bb-D  | +1&frasl;2-ſ |
| F-C   | &mdash;    | F-A   | +1&frasl;4-ſ |

### werck4.scl &mdash; Werckmeister IV
|       |            |       |              |
|-------|------------|-------|--------------|
| C-G   | -1&frasl;3 | C-E   | +1&frasl;3-ſ |
| G-D   | &mdash;    | G-B   | +1&frasl;3-ſ |
| D-A   | -1&frasl;3 | D-F#  | +1&frasl;3-ſ |
| A-E   | &mdash;    | A-C#  | +1&frasl;3-ſ |
| E-B   | -1&frasl;3 | E-G#  | +1&frasl;3-ſ |
| B-F#  | &mdash;    | B-Eb  | +z           |
| F#-C# | -1&frasl;3 | F#-Bb | +4&frasl;3-ſ |
| C#-G# | &mdash;    | C#-F  | +5&frasl;3-ſ |
| G#-Eb | +1&frasl;3 | G#-C  | +5&frasl;3-ſ |
| Eb-Bb | +1&frasl;3 | Eb-G  | +z           |
| Bb-F  | &mdash;    | Bb-D  | +2&frasl;3-ſ |
| F-C   | &mdash;    | F-A   | +1&frasl;3-ſ |

### werck5.scl &mdash; Werckmeister V
|       |            |       |              |
|-------|------------|-------|--------------|
| C-G   | &mdash;    | C-E   | +1&frasl;2-ſ |
| G-D   | &mdash;    | G-B   | +1&frasl;2-ſ |
| D-A   | -1&frasl;4 | D-F#  | +1&frasl;2-ſ |
| A-E   | -1&frasl;4 | A-C#  | +1&frasl;2-ſ |
| E-B   | &mdash;    | E-G#  | +1&frasl;2-ſ |
| B-F#  | &mdash;    | B-Eb  | +3&frasl;4-ſ |
| F#-C# | -1&frasl;4 | F#-Bb | +3&frasl;4-ſ |
| C#-G# | -1&frasl;4 | C#-F  | +z           |
| G#-Eb | +1&frasl;4 | G#-C  | +z           |
| Eb-Bb | &mdash;    | Eb-G  | +3&frasl;4-ſ |
| Bb-F  | &mdash;    | Bb-D  | +3&frasl;4-ſ |
| F-C   | -1&frasl;4 | F-A   | +1&frasl;2-ſ |

### werk6.scl &mdash; Werckmeister VI
note: Werckmeister VI is a rational tuning, thus does not conform to comma-based interpretation. For this reason, this chart uses cents.
|       |         |       |        |
|-------|---------|-------|--------|
| C-G   | -4.5¢   | C-E   | +8.7¢  |
| G-D   | -3.3¢   | G-B   | +13.2¢ |
| D-A   | -5¢     | D-F#  | +12.3¢ |
| A-E   | &mdash; | A-C#  | +11.0¢ |
| E-B   | &mdash; | E-G#  | +11.0¢ |
| B-F#  | -4.2¢   | B-Eb  | +14.5¢ |
| F#-C# | -6.3¢   | F#-Bb | +18.7¢ |
| C#-G# | &mdash; | C#-F  | +21¢   |
| G#-Eb | +3.5¢   | G#-C  | +21¢   |
| Eb-Bb | &mdash; | Eb-G  | +13¢   |
| Bb-F  | -4      | Bb-D  | +9.7¢  |
| F-C   | &mdash; | F-A   | +8.7¢  |

